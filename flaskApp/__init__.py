from flask import Flask
from flask import render_template
from flask import request, redirect
from bs4 import BeautifulSoup
from random import randrange
# from datetime import datetime

import mysql.connector
import requests
import datetime
import html.parser
import json
# mydb=MySQLdb.connect.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
mycursor = mydb.cursor(buffered=True)
# mycursor = mydb.cursor(MySQLdb.cursors.DictCursor)
app = Flask(__name__)

@app.route('/')
def home():
    url = "https://www.amazon.in/gp/product/B08696W3B3/ref=s9_acss_bw_cg_Budget_2a1_w"
    product_det = product_details_fetch(url)
    product_id = product_det['product_id']
    mycursor.execute("SELECT price,timestamp FROM price_history WHERE product_id = '"+str(product_id)+"'")
    price_history = mycursor.fetchall()
    # return render_template('home.html',data = price_history)
    return render_template('index.html',product = product_det, price_history = price_history )

@app.route('/user')
def user_index():
	return 'users'

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name = None):
	return render_template('home.html',data = name)

@app.route('/search', methods = ['POST'])
def search():
    url = request.form['product_url']
    product_det = product_details_fetch(url)
    product_id = product_det['product_id']
    mycursor.execute("SELECT price,timestamp FROM price_history WHERE product_id = %s",(product_id,))
    price_history = mycursor.fetchall()
    # for price in price_history:
    #   price = list(price)
    #   time = datetime.date(price[1])
    #   price.append(time.strftime("%B%d"))
    return render_template('index.html',product = product_det,price_history = price_history)

@app.route('/fetch')
def fetch_result():
    return render_template('home.html',name = arg )
  # print(soup_data.find_all('div'))


# for route search
def url_formatter(url):
  import re
  url = re.sub(r'[:|.|/|a-z]*in/','',url)
  # url = url.split("/")
  url = "https://www.amazon.in/"+url
  return url
      
def fetch_image_src(data):
    try:
      image = data.select_one('#main-image-container img')
      src = image['src']
    except Exception:
      return ""
    else:
      return src

def fetch_price(data):
    price = data.select_one('#priceblock_ourprice')
    if price is None:
        price = data.select_one('#priceblock_dealprice')
    if price is None:
        price = data.select_one('#priceblock_saleprice')
    if price is None: 
        return ""
    price =price.text
    price = price.replace('\u20B9','')
    price = price.replace(',','').strip()
    try:
        price = float(price)
    except Exception:
        return ""
    else:
        return price
      
def single_product_scrape(url):
    headers= { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:77.0) Gecko/2019'+str(randrange(10000000,99999999))+' Firefox/77.0' }
    response = requests.get(url,headers=headers)
    soup_data = BeautifulSoup(response.content, 'html.parser')
    price = fetch_price(soup_data)
    if not price:
        return ""
    product_name = soup_data.select_one('#productTitle').text.strip()
    src = fetch_image_src(soup_data)
    if 'http' not in src:
        src = ""
    rating = soup_data.find("span", attrs={'class':'a-icon-alt'}).string.strip()

    if 'stars' not in rating:
        rating = ""
   
    product_det = {
      "name"            : product_name,
      "url"             : url,
      "current_price"   : price,
      "src"             : src,
      "rating"          : rating
    }
    return product_det

def product_details_fetch(url):
  url = url_formatter(url)
  mycursor.execute("SELECT * FROM products WHERE url = '"+url+"'")
  product=mycursor.fetchone()
  if product is not None:
    product_det = {
      "name"            : product[2],
      "url"             : product[1],
      "current_price"   : product[5],
      "low_price"       : product[6],
      "high_price"      : product[7],
      "src"             : product[3],
      "rating"          : product[4],
      "product_id"      : product[0]
  }
  else:
    product_det = single_product_scrape(url)
    if not product_det:
      return ""
    mycursor.execute("INSERT INTO products (url,product_name,image_src,rating,current_price,low_price,high_price) VALUES (%s, %s, %s, %s, %s, %s, %s)", 
      (product_det['url'],product_det['name'],product_det['src'],product_det['rating'],product_det['current_price'],product_det['current_price'],product_det['current_price']))
    mydb.commit()
    product_id = int(mycursor.lastrowid);
    date = datetime.datetime.now()
    mycursor.execute("INSERT INTO price_history (product_id,price,timestamp) VALUES (%s, %s, %s)", (product_id, product_det['current_price'],date))
    mydb.commit()
    product_det['product_id'] = product_id
    product_det['low_price'] = product_det['current_price']
    product_det['high_price'] = product_det['current_price']

  return product_det

if __name__ == "__main__":
    app.run()