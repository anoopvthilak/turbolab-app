-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 21, 2021 at 09:45 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `price_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `celery_test`
--

CREATE TABLE `celery_test` (
  `id` int(11) NOT NULL,
  `time_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `celery_test`
--

INSERT INTO `celery_test` (`id`, `time_value`) VALUES
(1, 'works'),
(3, 'works'),
(4, 'works'),
(5, 'works'),
(6, 'works'),
(7, 'works'),
(8, 'works'),
(9, 'works'),
(10, 'works'),
(11, 'works'),
(12, 'works'),
(13, 'works'),
(14, 'works'),
(15, 'works'),
(16, 'works'),
(17, 'works'),
(18, 'works'),
(19, 'works'),
(20, 'works'),
(21, 'works'),
(22, 'works'),
(23, 'works'),
(24, 'works'),
(25, 'works'),
(26, 'works'),
(27, 'works'),
(28, 'works'),
(29, 'works'),
(30, 'works'),
(31, 'works'),
(32, 'works'),
(33, 'works'),
(34, 'works'),
(35, 'works'),
(36, 'works'),
(37, 'works'),
(38, 'works'),
(39, 'works'),
(40, 'works'),
(41, 'works'),
(42, 'works'),
(43, 'works'),
(44, 'works'),
(45, 'works'),
(46, 'works'),
(47, 'works'),
(48, 'works'),
(49, 'works'),
(50, 'works'),
(51, 'works'),
(52, 'works');

-- --------------------------------------------------------

--
-- Table structure for table `price_history`
--

CREATE TABLE `price_history` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_history`
--

INSERT INTO `price_history` (`id`, `product_id`, `price`, `timestamp`) VALUES
(25, 1, 10999, '2021-04-20 22:33:19.437253'),
(26, 2, 600, '2021-04-20 22:33:29.862984'),
(27, 3, 885, '2021-04-20 22:33:34.432642'),
(28, 4, 16490, '2021-04-20 22:33:38.758543'),
(29, 5, 16999, '2021-04-20 22:33:42.576158'),
(30, 6, 949, '2021-04-20 22:33:46.230385'),
(31, 7, 6799, '2021-04-20 22:33:51.114305'),
(32, 8, 29990, '2021-04-20 22:33:55.459819'),
(33, 9, 14975, '2021-04-20 22:34:00.146494'),
(34, 10, 6199, '2021-04-20 22:34:03.542272'),
(35, 11, 839, '2021-04-20 22:34:10.419819'),
(58, 2, 600, '2021-04-21 20:34:04.191214'),
(59, 3, 885, '2021-04-21 20:34:04.942008'),
(60, 1, 10999, '2021-04-21 20:34:04.956654'),
(61, 4, 16490, '2021-04-21 20:34:05.276880'),
(62, 6, 949, '2021-04-21 20:34:09.313019'),
(63, 5, 16999, '2021-04-21 20:34:09.877319'),
(64, 7, 6799, '2021-04-21 20:34:09.947523'),
(65, 8, 29990, '2021-04-21 20:34:10.253434'),
(66, 9, 15000, '2021-04-21 20:34:12.588528'),
(67, 10, 6199, '2021-04-21 20:34:13.374654'),
(68, 11, 879, '2021-04-21 20:34:13.470612'),
(69, 1, 10999, '2021-04-21 20:59:17.611443'),
(70, 4, 16490, '2021-04-21 20:59:19.616043'),
(71, 2, 600, '2021-04-21 20:59:20.064719'),
(72, 5, 16999, '2021-04-21 20:59:23.055664'),
(73, 3, 885, '2021-04-21 20:59:25.588305'),
(74, 7, 6799, '2021-04-21 20:59:26.261226'),
(75, 6, 949, '2021-04-21 20:59:26.405191'),
(76, 8, 29990, '2021-04-21 20:59:31.033312'),
(77, 10, 6199, '2021-04-21 20:59:38.001160'),
(78, 11, 879, '2021-04-21 20:59:38.049636'),
(79, 9, 15000, '2021-04-21 20:59:38.402728'),
(80, 4, 16490, '2021-04-21 21:34:28.958554'),
(81, 5, 16999, '2021-04-21 21:34:29.820212'),
(82, 6, 949, '2021-04-21 21:34:38.594057'),
(83, 3, 885, '2021-04-21 21:34:38.708276'),
(84, 2, 600, '2021-04-21 21:34:39.360326'),
(85, 10, 6199, '2021-04-21 21:34:39.363358'),
(86, 1, 10999, '2021-04-21 21:34:40.002351'),
(87, 7, 6799, '2021-04-21 21:34:42.809766'),
(88, 8, 29990, '2021-04-21 21:34:48.468964'),
(89, 9, 15000, '2021-04-21 21:34:49.494622'),
(90, 11, 879, '2021-04-21 21:34:51.067765');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `image_src` text,
  `rating` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `url`, `product_name`, `image_src`, `rating`) VALUES
(1, 'https://www.amazon.in/Redmi-Note-Pebble-Grey-Storage/dp/B086977TR6/ref=pd_vtp_4?pd_rd_w=bDiTw&pf_rd_p=51dddd6c-e5d9-4907-af32-f0b367217ee2&pf_rd_r=5BQTZ36C4ZJ47XBNV5F0&pd_rd_r=70403550-6c5e-4641-965e-64dff7c48d35&pd_rd_wg=vUTqS&pd_rd_i=B086977TR6&psc=1', 'Redmi Note 9 (Pebble Grey, 4GB RAM 64GB Storage) - 48MP Quad Camera & Full HD+ Display | Extra INR 1000 Off on Exchange', '\ndata:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAwMDQsNCxAODBANEA4QExYRDRASGR8dFhsVHhgYEx4YFRsVFBwYGyAZHhsjKyQpIyA6LCYxGSYoRC5FOUsyLkIBCA4NDhITDhERExMREhYTJxsSES4cHR8TKQsfERYeFhcfEBYZHBAXIRcpDCMRCy8gKBwUJxYSERQeFg4bHTAeIP/AABEIASwArwMBIgACEQEDEQH/xAC+AAABBQEBAQAAAAAAAAAAAAAAAwQFBgcCAQgQAAIBAgMEBAgKBgkDBQAAAAECAwAEBREhBhIxQRNRYXEUIjI2QnSx0RYzUnOBkaGywdMjU1VicpIVNUNUY4KTwsM0ouEHJCXS4gEAAgMBAQEAAAAAAAAAAAAABAUAAwYCAQcRAAEDAQQFCQUHAwMFAAAAAAEAAhEDEiExQQQFUWFxEyIycpGhssHRQlOBsfAUUmKCkqKzFUPSBjPxI5PCw+H/2gAMAwEAAhEDEQA/ANVoooqKLiR4oY3lmdY4o1LSOxyAUcyTVAu9uj0rRYJadOBxuZtFrnb+/mnuYMGhYrFkJ7ysujikvN9iwitIuXZ2CrKdN9R1lgk/UknAAZq25sXAkid1nK7vWhHarax+eFQj+OL8ZWrn4T7Wf3nC/wDUhqhQw2DkKUmAckRSvorEchlShs7ZWyZD9ZpkzVtR4llSm4TBNo4qcsdjP0t9FefhPtZ/ecM/1IaPhPtX/eMM/wBSGqYtnY84z37x99KiwsPkH+Y++r/6RX+8z9Tl3yr9lP8AQ30VufanatFz6awfsR4S1IfC7a3qg+uGq2MPw/nGf5jXYw/DOcR/mPvqf0ev95n6nKcq7Yz9DfRWH4XbW/4H1w0fC7a3/A+uGq8bHCx/ZH+dvfXaYTayarAVX5TMwHtqHU9cYvYPzFcO0iwJdyTRtst9FPfC7a3/AAPrhpYbU7VEA9Pho7C8NQyYHhnp/YxpwMAwkjRG/nNDnVrx7be13olTtb0BsO8U2+YBUj8KNqv7zhn+pDR8KNq/7zhn88NQ02A2aapGT2Fj76ajDMNcFOhZJRyLN+Jq5uqarhIew/mcjqOmsrAmmaZ2iw0O7CFY/hRtX/ecL/nhrtdrtqk/Zc3c6exJlqlvYWitkYz9Z99DYdZPHmodD1g/gauOpNJAkOYfzFE8o7Yz9LfRajhe3FpczC1xmDwGVtEn4xe9Kuor5t6KSGdbWZt+GU5RP1E6AjqI51suwOJTXOHS2F0S01gwVCecRz+4VNIXsfTcWPBDgYIzn6wXNxBIEEYjKzt74Kt1FFFcKpFFFFRRZHtKxfajFX/VWbZf6US1XGtQmHxNHnuNEFny6yMy311YtoPOTGvU2+5FUdhThoFRtRugEVpNUATpLiJAbDh+AudIV9Tpflb4GqJgtpjEiu8Zt0YOAmeeY7CAFLabx4ndAqQMQdev94fiK6uLeSyl34tYWpWPclG9Ed1uYrU0tFo0mHkpsOMzibWw8MAqXUpFyYtA66rqK48ccVapcEqf0ifSKXRYn4PkephVklu8IU8s24Br+4qDHSngrU4jt5n8okDqUZn3VOLbycVMR+iu9y5XiB9FcmscoCW1tI0poupFu/H1Cj4rcpqkR3vlNqftpYx3HFkY06DsOIFLo6t2d1DOJmSJWVq13kkvBcdpJUZmF8tMu+lky4oxBqSIB0cBlPOmM9tuDfi4cxXIc11xuPchA9rtrT2hLxz67kw7jSF7aby9JHkHGqmkkcMMn4faKfW8m63RSaqfJNVkFhtN7NyjalSi8PaYc3sj0PtKuzr0kfSAZMNHHbTWM65cjU9dwdDP/hy6HvqEkQxyEHkaeUqgcIGESF9JoVm1qbKrcHDDY7MJneL+jU845EYfWBWgbG5JtRicfIxE/bEaod38QT1lfvCr3sl53X/zH5NYXXIHL097BPG05Gsxf1D5LRKKKKzqpRRRRUUWRY/5x416m3shqAsH6NlHIqpFWDH/ADjxr1NvZFVWibJI26gAe6tbqMSdKG4eJyLI5zuq3wBXPcSeEowBDCq1Ij28xXqOlT9i+9GKjsV3ROOsitLQJa91PIq1iUgm3xuvrSrxAeMtM7SNjkeVP2fe8SMFjzPKvX3Ou+OxSpZi9K20jcDUqhzGVRcMBTV2UU9E0CcXz7qXVQCeaJS0ua0c4hoRNFzFNOBp09xGwyXepqTXrLUXrE6byJcbBBTyNt5d2lF1BU02ipzweh3YlZh2KiJ06KXsNKjx0y5rqtL3q+LnTWE5EUYDaaDmi5tMBzCfOBdWZ+WlQd3GWjSUfwv3ipy3YRXG76L6U3ngyM8I5jfjFcUn2XRliOqcVoNUaRZdUoHouFtnWwcPrYqpdD/2zdhX7wq97J+d1/8AMfk1SLwZWzHtX7wq77J+dt/8x+TWd1xfWpdQeNy3TPa6hWiUUUVnVSiiiioosix/zixr1N/ZFVVj+K/yirVj/nFjXqj+yKqpH8SO0ACtfqHpaTwHjejT0ndRvgCs2FkmOo+VvCrx2Hkg5DuGlLdJ4Nh/78nioKaxkQw9rVrGt51R+ZMN81wHgJ+XUDcU5IOPbSiSnyYgAKi1Yuam4lW3jDN5R8kVTUAaBmTlvSzSa5Y0meccEoIwBnOxJ+TST3cEZ3URWbqH4mo6e5aRiqH+JvdTYMFO6ozaqxSzeeDcllTSc+X1CdwUv4dcHgsaillv29NI27jUSoGYEhZ3Pkxr7hUpFb3JAO5FEOpuP1CqXimMgPrtQFVlJuNlv12lO47u3PEMn0Zj6xTtCj+MrIw7DTAQSDisL93imuRCpbxN5JPknRvoPA0GQw4GN+I9Uqcymei7z9Cnd0R0ZqPjrpklJyLE5cQeNeqvKiGgBuMqwQ1pEynD8Fbqp1cf2E4/hemz/FinijpbJl5jUUGTFk74PVKGp1DTfSqDJ37c/mqni8XRxzjlvKw+lhVo2Xmih2tuxLnnNH0cX8W7E9QONa4eZOsID3h1qVwDzwbv/wCJKzutHS+ifwAfG2V9a0F4qNd1HdzZWo0UUUmXSKKKKiiyPaDzhxr1R/ZFVXtk3zEg4ZAn6qtG0HnDjXqb+yKq1EeitgR8ZIoC9gyrW6ix0ngPE9EVOkeq3wNTiR/CLkAfFReKv4mm00peTJeA0Ferv7vQ26PLM/oRgs31LTqDCMY3wz2N6o6zE3urb2qTek5rYHNBIBVCeWUSovSSeSup7Wptd3bOxUHjxry6ulT9BHmCmhUjI59ZFRTNlpxZqoa2SXuz6PBK3t5Rxceg3DrJ3vZZInGnVrDLMxSDTL42ZuA957KaWkDXDlEOUa/HS/gvWTU+rIqCKAbsS0PUcRhik+lV7HNbBf3Abx4U7gSC2XdhGbnypDqxpYMxpmHAGlKKWalThnmsk8EkuJJJxOadg12QGGTjMcusdxpsN8UqrEceFUQhiCMEp4pyWbMjgkw4jsavHieI5sA6cnWu9CNNQeIruOQx6HMxnhny7DXMnL4hc2imbtvZAVJWo/RHOvTBE3jLpSyqEGVVveCAAqXPBAAVWxhf/irgfIdftcVIYB53t3/8SUzxr+r7vtC/fWnmAedzd/8AxJWf1l06fAeJfU9RPtUqnB38S1GiiilibIoooqKLI9ofOHGfU39kVVuyglxC9tLKEhXnKRhuSrzb6Bmasu0HnDjPqb+yKoTZidLTH8OnnOSF9xieW+hiH2tWm1Q5zW6c5vSbSlvWl6srGHAbWt7LDVpl3eYJsnZxQxQkySjxI0y6WTLi8rmn82ORJgUOL28E09o+RuUQgSxjMoSQdG3GqC2w2fvr69gvbBOnCRiCeHMBtGZwVLZDXfqTtbNsH2MvExEpvtBcyToNVBdSoiFDuZoppaPUtmrXfU/6rZvsyZEYjKDvVN65v7DB9rMH8LsNwXYBFvPlk4kX+xnrGreKS4lKnNCPjWPoj31p/wD6eRXEOG3c7hhDcSqLf/IGV3FUF5o5bm58H0iknlkDdYLEitFoFplTS6DXF9OmRYOycvI5SCh6k2ZGPntTxSioIoRuxJ9vaesmnEW/K25EKb2kEl0+5HpGvltVqjt4bWLXIKNTmftY0ZWqNZdi5ZWrSJJ+fmSmUNqB++fqUU6EYA1YfRTeW+XhEpbtOi00N1cngUHcvvzoHnnclTmMzMlSe71N9dcsnWMj9lRy3dwPKCMO7L2U9huY5PF1DfIb/aa4LSqHUWnC4roZofbSx1G8vA8RXhA5cK5GaHsNVlK3tIMHHyXaOY+GqeynJlUrnTYg8VpPxT1r1ivLIKqgFReM64ZcHrA++tPsA87m/i/4kpli+uF3J5AIB/OtP9nUkfa6QxqWCHfl7F6JBmazmsenT4DxFfTtQEClW/N/CFp1FFFLE7RRRRUUWSbQ+cOM+pv7IqpUamQKuWZIAq67Q+cOM+pv7IqqAboIVVdZnUf5VIrXaiMHSeA8b1bVItflb4Gq42O2eJWEQtJ44r5IwFErkrJ3MwzDVDY7tLieNKsEgjgtVOYgi5nkZCTm1V8ncXIcTxr2JWJ8RWZ+oDM1pm6Fowqco2m0OxLsp2x0RuQ52E8fT1UnaX+JWVvLa2tzKkM6lJ4+K66Hcz8kkcSte2drLdTrBAO125AUxjWR5FjjBMjndVe2tFsLWPDLUIMmuH1c9vuFXaS9lAGw1oqP3YmOkdsLotaRCWSO3w+BY4xm/Icy3X3mmEyM7b10+5zWIan6vfUg6Oj5DWdtXf5APIdtISR20C71w6jPm51NZlrr5xJzzWW0skktbh5qPztRwRz3mjO3PoMK7N1hzHIN9O6fdR0cEmsLKe73URfnaCzT2uGMpPoY31ibXqNNnjZTkwyNLujofGGY+UK7EgIyk8ZeTcxXQcqgXC8c4Lu2uTmI5TrwRjz7Gp8R9XKoiWLdGY1Q8DT21n3x0ch8YeSese8Vy5uYXb2io38QwS6kocjwrpkVhXrDPSkwSlVhKCDJyKj8ZXLB7nuT761L7I+dN/8AM/hDUNjbZ4TP/k++tSOzU5g2snAUMJx0Tk8h0cb5is1p/wDuMnYPEV9H1CCaNfrO/jatKooopYtCiiiioosk2h84cZ9Tf2RVR1O6gZtWIFXnaDzhxn1N/ZFVBTN8uoAVqtSTOkAbB43K2secNtkR/wBtqm9nsLOM4xDauWWHV7hhxEa1r+F32DQ4u+BYdb7ht4y8hQAICuQKsSS7vrqTWabH30Nhj8RnYJFOjWxfqLZFfrYVoNhgc1htXd4ojo9rcpKwX01ld1dgaL1kAKj2VXODRQmiPZOkWr+zJDet/BRWyuE2bzX897FndvJL0DNwSIsRmo5NnSkKPGHmn8aYMUXqLDTMdi1YcOsJrWa5urp0G9mFRT4qRbxfU82aogHwi4LAZJmSo7M8/tpby5qPrGZbA4CG9EeatgYBMrueLDrJ7mbVj5A5s54CoWCzDhb3Fy8k8+sFqvJeRal7wpiO0CQPrZYYpmn6i4yOX15CpKF0AfELrVpDlCnsApiCWNGNtwl22D0GDZMWqmeASGuwl7abLnOxdsYkltmZfFsbcLyGWvsptLaWm94yS2j8jxTOplLjFCN8Ww3OQ506ivLac9DdJ0bnTdkGn0GhjUqNvgEZw6T2Xqh2rARzKptd0qrutzbj9MBLFylWkWiBG/ARlzWrXNhrx5vYt3wP5J7qgnt1Mh6MGCccYW8k/wANXMrNdn6/EfQWYraPVouh7Y2O9gqNjkKk8x6aGh0yAkiJyzzHWppaSPfYhgY5hxBpurPE+oyPpKeBFHBDjaLjmFJwSiZOp10YUqQCKivi2E8Oq8GH4GpVHWVA6c6qcIvGC4qUw8WhiMQojGUywm4PYv31p3gHnaf4/wDhSksb/qe57k++tLbPhztY5VWYId6QjkvRIM2rLawvezgPEt1/p/8A2q87X/whafRRRS5PEUUUVFFku0HnDjHqb+yKqRGMkUDqq77QecOMepv7IqpcMUk26kfEgVsdQRa0o7GjxvV9Qc7bzW+BqksGwyTGcQSzidEzDM7t1CtIjvsUw65OGLMssVoEV2cb2eahgilsm0BqVwjE8FmeCwso8rsQjfcRBfJUBs3FSc15hlu0qzp46EB23BmSQCN0nU1Vpmm1KtSy+gbIbzKRxtT05ib8AuBIkESc1GjwzEUaOWdVYANIuWgU8AAvM1FySixs555AM4VZiO0aAfSan1xSwGfRJI0mWe7u5An96qDtbdlLNLf07hzJJ3D3tQui031Kgplthhdh+H2vkuSl8OsxHgvhKxXlxNieTzzLoq6sSMtxjuo3Ek61JQIsk+ZyaO2ASIcjJXEcUVrbWFqRbeDwWvSXRLfpxPu73iDe3/L7MqdWimO2UnyiC5/iY1e55PKGZlxg/hkgbgYZHAhIar4qPOeG+Bj87k6LqreMWL8yOP2HSl2SK4TKVRKvJv7Qdx59xpqNyMbz+Mx4D8aVjZWOcR3JOanyWoNwzEiM1Yx69Xp7IDIme15H0l91OpYbO/izOTdTjywa6ik3s8tH4SI3A9jD8abvA8bGayJVl+NiPL3ih5k42H5Oynf9Qr3EOBDgHtOI9FDXllNCAJg0sPoTL5a++oqWEMo3iGU+RMv49Rq6wXMU43WAV/SQ8D3UwusN1aS0yVj5cLaq1MKWkkGy/mnbks3X1fHPoG037uapZ6SB9RmDxHJhSsb9EekTNomPjjmDUjJAGzQqVI8qJuI7V6xUY8clu2Y8ZD5Q6x206D2u4pLJB2OXWMsrYLcMOYTI/wCdaldkfOm/+Z/CCq5iRIwqfoznE+7mDyO+tWPZLzqv/mvwhrKawEVGcB4itvqmOSrxtPbZatCooopWnCKKKKiiyTaHzhxj1N/ZFUDZKVRI4RnK6gseqp7aLzgxn1N/ZFTfDbYCFFQ5ZorTynkMq0uqHhv2ngPE9Fnpnqt8AUxhdo5kWK3LB2P6WUHI58xmKu8mH4bGiNeSl3GgaR91c+zUE1XtnriN8WWKLJYVifo2PFn0zP1Z1zikc39NTy3oO5p4GD5O5kPJ5d9d1+UqVi0uNOGWp9sycO9cEycYU9JhcKRmWyY5nximeYI7DWXvL/TG00O5rEki7nzcfjk1qGAPJlOTvC3zG51F+e5VE2et4kusSxMZCBZJkterc3ixIq3RHmn9rtG29rA1jvxOuj62FDuO3DE8ArPJ0893JGouisselwAvRAkElQOjzyBGR8fnSoXIBSMt3Ug/UKYWIQxxOOgVpV6eQO7mUufSADbgB5Cnzk7naxyzNAxBgYee1ZCrUkznPco+8n3FzHlPondUULlo2+M8bmDXN9ODI78l8SOogZFd+U5A8BWgo0BZEqi9xlXS2vUnyDMEmHkPyPY3WKmIpd/jmkqcese9TWapM0ZzjJZRy5juqy2OILMFDOFkXSKT/a/YaX6RoUC03BN6VQmGuxyORVjmt0nOa5R3A1yHBu1aTjupIm6O6B04NzruKZZBuuMmXyl5r+8nWtLvuOoE4Dr6Mo4/TSTDmuEjLaOBRUEGRzTns+IXM9vb3aAniNUlTiDUFcWskR3ZwCD5Mo8k9/UalTbzwnftn3l7KUS6RwY7pMgdDmNKuY9zeibbdntBBV6FOre4WH/e9klUDG7VoMNndcwh3N4f51qa2S86r/5r8IaW2nteiwC7khO/BknevjrSGyPnTf8AzP4Q0FpdTlHMduA7ymGraTqbK7XDb2WW5rQ6KKKCTJFFFFRRZHtH5wYz6m/sipsJl8EiQeJAETNRxY5DU052i84MZ9Tf2RVxg8VsbyyN6UaI7nSh/IC7vpVoNVED7Q4gmy2Y3y76CvqdI9VvgalbJ5Unju0fomhYNB7iOYIq8DaTBVtg2JIYm5grvqT+7Thvgch8d8MXvce+kRBsXezrEi4dcy+hGjbzfY1W1q9GsQ6pRrAN9oATZ4m5VEqv3+1IxAf0ZgEcqyz5pJduMgiczEtcX4isMMiw+19LKNfxY97VdbbCcGtC8lvaW1uMv0snZ3k1nSO93dvdMRuBjHbfJ45b3cBVlB1F91FjmU2XmTLjVyJI2RdlilOmVrFMj2n3DgrVYx7lsiBncLGFRgibqjdLatu7+QI1JPGmV5N0UbkeioRf4jT6KJIIFVfBjupnM29mxfi3MDjwqs4pcZKAT5Ocrd50WuaDLdQ5iVmakl1NuYF/0VETMJJRHn4kYzkNNy++2+R4vCNfxrg5qgjOjSfpJz1LyWuo1Lt2DQCteAAOHy/+5Ih8NEdvBKKrPrz5VwxlhcMND9hqUhiOiga1LLhySx5PQr9IYw34eSCbXgxEtTWwxNZAqSMVdPIfmvvFWaG7GYVyqO2qn+zftU8jVGvsJu7T9LCC8Y9JdSK9scTOXRTZMDxRuHevUaAraJTqg1KJDhmM5+voLUUKzXgBx4Oz4FaIGVToWib7D+FKnxxlIqSDrGhqt290csoJQR+ql/AnQ1IrM/pQjvH/AINZ59BwP0D6op1IjC8d3omO0sYTZy+MZdVKpvRt84lM9kfOm++Z/CGnG0kpbZy9XJxmqffSm+yPnTf/ADP4Q0orAyJRWjCA/qnyWh0UUVUu0UUUVFFke0XnBjPqb+yKoyOWRoI0gjbVFBOWvAVJbR/1/jPqb+yKpTCZ4JZ7C1GQSURpKIky9Hm5JanurKljl3WLcCfgC5e16rWPbPtBoHGw31UZh2zOIYnJm/6CD+0nf2KKv0FvgmzdkSoSLPR5W1mlNToVVUKoAUDIAVDz7NWc9wbm8uLy4lPDpGGQ7FUKABUq6e/SDFZ5ZRH9sKmoakcwAu7lUsQxO+xxyrb1thwPxY4vSloidLG5QdBEy7sY4HI8KtLbP2OQBkn/AIQRkP8AtqGjULpkAFJAA5KOJ76LbXolpZSBaBluO/EkxesnpLK7SH1SCThmJG7BOjuRM8skkbxuH4HNnBB03eI7c+GVUS8kEs5ZwSq/pXX7ET6auN0ls+Ua+FZyQPIjjLcVFUuS5y4HLI1Qp3PA+WTvv/FloP8AKvtphoLRLzJmP2X+kHeAuWMI5xAGOZPPMTee75lcLvOxz1Zjm57amLeA6ADWmlpDwY8atVlAqDfemOkVrIhKq9UklvaV3bWqxLvPSryFtE0WvHcuczoo4CmctwiDqpIA5xk3lCWouTgSdHqGI6xyqLvcLtL7OSApBc8cx8Wx/eA1XvFN5rtmzCimgmnQ5qxFMadGo02musO+sQi6VV7TLbkzL3ljL0N0jKRwz5jrU8GFS9tiHAB2HYDlR4ek0fQ30KzRfaO0HiKanD7eU52M6/MzaN3K3A0Y4tcIrMsH7wvZ6j5b1qKOmNdAJsHYej2p9jN2ZMBukLscwmh+cWn2yXnTf/M/hDVVxKK6gsJVmWRASmjcD46/QatWyXnVf/NfhDWJ1jTayoyyQQRPeVoKBJtz908MAtCooopUoiiiioosh2k/r/GfU39kVTFuY47WDo1CnokzbnnuiofaP+v8a9Tf2RU+hYvDAq8okH/aKb6vEmr38LRVOmMDizcB/G1Pd+Qn4yT6zS8aStrvyd5Y0RoiZb2p03j1DnUqXwU3JG/P4PuDdK559J7eFNajwLg0nfE/WNySmnvA4lNBmvB3LdZJriRwFPHdGrnrPUO+pQDZ7hv3VQdwclOXAfFjr7TVVMhx6LhxEJdUpEkc5rhuMom6OSxZnkC2aZPcnpdS+456Po9/0HCZALrVJhDTS7zVa7m5SZkihe0ivzC4RBa5kskbEhX3sgxXQHKq9YxjcBHE040WWNqzIMgxxm/fMY4XAQIVmkENY0jZAKnLGDfYdQqYkceQPJHGm8QEMIA8o0lLIEUk95oR0vdPYssUXE4RfZUK7NI2bVzJKZXzPD0RTuCIDxm40c1oYFIs3nFcJblhm2YpTwVR1mnygClVQtqdBVZquVQe4qMNqh9GkWshyJFS7dANCzVznbfKaoKz96Ja7eq9iQvI8KnQyFoTuB1P8a1YtkvOq/8AmfwhqKxp4DhFwE397xPvrUpsl51X/wAz+ENZbWBJqMJEXD5lb3VRmlVvnHwtWh0UUUqTVFFFFRRZFtF5wY16m/sipbDX8VM+UafdFI7Q+cONeqP7IqbWsqxtDkf0csShT25e+n+q22vtHV/8nLute4D8I8DVYWkJIX6+01OYSkCtM0qLJMsZeFDqT1hQedVS1fOfJuRJq24K2c85Rd+dYs4fdnyLUw0ttmm4bvMfRScjnsG/y+iE6GJx5a2Vt2jIf/Wq7cHUnlq2VWo4niuWlsuf8D++qpPmXbd8onLLtz4UJo4gnmhv5p/4Q1XAc4u/LCTeHEJujs44sVNvNbFzexMeiU7rOUCBMmXPxWBbM51FWcTpN0UqGN49HjYZEHqINLzWtsl1LI1ncSLKCLlmgkIiCI+8yP5G/LIV3WXRAlNsObjmaZ0ujUjA7v7l8mbR4HAXAgCUv0wc2mN3dcpiSTXu0FQ95OSwjHe1O2eoJ3zmcnryomm2L0jayXHcpG2TPx2+ipRMgu83DlTCHgiinbOOkPyY9B31y8yhHAucU5BCjfk+haRed3Omi0weYu2ZOldK2eZJyQcTXgYMSpyZTjTPrNd9G+WZAXvpg98iaQgdrU1a7djqxPdVwY47gvRRfsgJfGFAwufx1PkaD+NamtkvOu/+Z/CGqjfuWsZND6P3hVu2S867/wCZ/CCsprJsVKfVHiK3eqGltKsDtPhatDooopOnKKKKKiiyPaHzixn1N/ZFUDDmI2tidUCyQnvAJFTu0PnFjPqb+yKq6GIe0m+VCgb7prV6jEu0jqiOsHOXtfpt6o/japWC5yKynylIWQfZnWlWOG4vZO7o9sOkUAkHP2rWThjDMSOHMdYq8bKnfuLuITuLmW3YWDuxOR1DEZ810pvrGk6wXtIDQLxE3Wh2WcTuVTmNdBVvZMZDfGW+Y45j/wDNVOUZSNpmQ292Eg1NYXa4pbSu+JdHEiIyynfzVxlx7BUA8sTHKPPItlB15E5KKQaO2HVACxwAHOAuzzQNSmXAYzsJlNsQEPTi5eKwSxlgbwqcuRMtwEYbqIZs2beyyAQ51X7Sfo3Bbg1XaePestyffhhMW5NG1v6ZBXeEjJxas/uLaWxcJId+BviZR7D1Gm+iw4PE7hj0RO24m++LoAQdelaDbrwp1nFQ9yCkufovwPbXkNyVPRyHMHyDS0u66lG58KMAySjki0z28E5t581VhxU6ink5ylfLyZVEkZqtwyNFJuty0b8DU2HMlm3N7c9IvzZ0YfRVLhBBQj6Nlw2FNt+vLmXKJVFNZGyc5cDqKSnbOMGrwixRmF5vEnLlXQlA0Wmgakt/WoXI3kU7u5i1q69ZX7wq87J+dl/8z+EFZtK+cZHav3hWk7J+dl/8z+TWR1mZqU+oPEU+0FlhtUbQT3BaHRRRSZFIoooqKLI8f848a9Tf2RVVjrZ2h6gw+2rTj/nHjXqb+yKqsf8AobUdjGtdqHp1vh/7FNI6TeqP42p3JqQesVN4FZjEHlE03g9rZp09zOPKXqCVAsf0aH90VL4DilnYXFzBfqz2F9F0N3ucU6mrW6SH8jU5MS8YbYtCYBumJsb16MArdc2Vvf3djZw3194Jd2r3EZnZnLsuZGjZZVVVneOToZ8kKtuO3JSDkW01O72VcXv9msOS0vILh7ye0tjbWEY9rndFUi6V2zkOReQmQn946mkGhh5lrg9rIhsiDytp0nfcRanMQq8wpqK6t7OVpf6RN14joIESUdJvKVCuZVVQtNc0uIDHKAykeMPxFcw2VrJYJKVYRKA9xfdJoCY5WZN3gvQuqKF8p86i7adlIDaZ0VTpscahaXFwIDiQBhMQGADeZ514mF05oKYzwtBIYX1HGJ+yuoZiylX8pdDUleRiaL94aoe2oBmKsJBx4PVrgYBSx9IfBOJ+T9Xiv3U/sbkRyI76qPEmHWh0NR5YMOwjI0lE5jchuWjd3I1S4SgKlGWluYvHBSF5E1vM8ROfRnxG64zqrDvFMt7NStS5C3luibyi6gGUJY5CWL5GZ9JfRqHeG4jbJo5VI5MpBqprsjivKJBEHFIq3EUk+hpR1bPMhlPaK4OoyYfSK9KYgBIOeHay+0VqOyfnbf8AzP5NZe6EZHiN5faK1DZPzuv/AJn8msnrGeUp9UeIpnQwf1T5LQ6KKKVKIoooqKLIsf8AOPGvVG9kVVltLW2H7lWbH/OTGvVG+5FVYf4qD5sewVrtQ9OvwHzeva/SbwHgavXJ6GMA5E6VqN1iVjs1PYYRBZJLFOiG4c9TOYt46HfYkEmstYExJ2NWh3O0eyF7NFPd2F3NPCqiJyo5Hf5TU+1hSc80BydSrTFu00e9gWCeGIXN8BP7yDZXZ+9klZTNeSHpYLMaiLtRfIQFubVSkLXAKEkSSvmvPJiewZnLsFWmfH9jb65NzeWF1JOQFLlPdNVQt5xbYlBcOWESTpMd3ygiuHyHblQmiU6obUL2VuVDBe7MiYa0fNQiU9uPBo4wlrdYMAkWVwjQs0rz+myvJbls2PDUZVDZ5oCMwR18afXd7A0kpXDsNAlLFHyl3hnwb/qMt6opGy0zJpvo1NwaS4OBN5JszOd7fO9WqTil6SLXiNDUTcrk56m499Ko/Ry6+S1c3H2c/fVrqeIyKHeEyR93NWrpjnqDqODCk2XePPPrFJnIaMxPYKWuYWyhbIuKV6dk0D0eFXJ4TP8AXXAy+Q31V4RGeOneKEMqWKeYHYE5W+vE9IsOo0ut5aSf9RCoPy08U/8Ab4v1io7oh6BrkpKOKhhVfOGUqs0aRwhp3XFSdxDAbcywS5gFSUbRvKHAjMGr9sl523/zH5NZTwYeUpLLmOR1Fatsl53X/wAx+TWY1g6ajOqPEUfo7C0PBJPNMbYgLRKKKKVLtFFFFRRZJtENzajFUPp2jZf6UTVS7WcTRRxH4yMFQOteRHdWi7fWb2uI22LIM4Jk8HuT2+50+5WSzRPDJ2cY2HAjkQaP0LS36NUtgWsnN2snb8lZUvLTtaO0NAPhVh/sh2GuV41BC6uQMhI+VHhd1+satmNe6PnTqd3quQQArAhypV2zTtU1W/C7r9Y1Hhd1+savDrzRvuVe7/JSVYi29HlzHCm4NQgurkcJGo8Juflmuhr3Rvd1f2/5L2QpxzmO0V7v769tQXhVz+sauRcTjg5qf13Rfd1f2/5Ks3qXKHPTepRLdj1KKhvC7r9Y1HhVz+saqXa50U/26v7f8lTYKnDbScmNJNFcLzJ7xnUSLu7HCV668OvP1r0C7WdA4Nqd3quOTduT89KPLjVu7Q16skefF0PUwzFRpu7o8ZGpJppXGTMSKHOsqeTXHsXnIypOeVWkiiUoxLqWZe/QVqGx/j7VYm/VER9sNZJZoelWZxlFEQ7Hu1AHWSa2fYKzmhsbjE7gZPfPlB2oCxL/AEsaz9es6q+2btg2NCMpNDWu6uP4yR/z8Fd6KKKoXiKKKKiiRuYLe6gkt7qNZYZV3JEas3v9hL+B2bB54p7Y6rBPo3s3GrTqK8XYdGQI2ZSsXbZnaKPR8Gtn7Vy/2XNc/B7Hf2HF9v51bVRXMHau7TPdt7XeqxU7O48ACcDg14f+cpqPg9jv7Dh+386tqoqQdqlpnux2u9Vivwex39iQ/b+dR8Hsd/YcP2/nVtVFSDtXtpnux2u9Vivwex39hw/UfzqPg9jv7Dh+o/nVtVFSDtUts92O13qsV+DuO/sSH7fzqPg9jv7Dh+386tqoqQdqltnux2u9Vivwex39hw/b+dR8Hsd/YcP1H86tqoqQdqlpnux2u9Vivwex39hxfb+dSibL7SS+RhNpF2sV/wB09bNRUg7V5aZ7tva71Wd4XsM4mSfHp0dE1Szg9jtktaGqqihUAVVACqNAANAABXtFdKsunYBsyRRRRXq5X//Z\n\n\n\n\n\n\n\n', NULL),
(2, 'https://www.amazon.in/Shree-Radhey-Organic-Ghee-300/dp/B07S9R4JWJ/ref=bmx_4?pd_rd_w=pBO0e&pf_rd_p=e1cc32be-bd98-4c8f-97f9-d875240632c8&pf_rd_r=KN190GPXKC2DM99FEZY8&pd_rd_r=8ebf5d20-f3b9-4f6b-8c42-328bdbfc2b23&pd_rd_wg=TXx7N&pd_rd_i=B07S9R4JWJ&psc=1', 'Shree Radhey Organic A2 Gir Cow Ghee -300 ml', '', '4.1 out of 5 stars'),
(3, 'https://www.amazon.in/EcoSvasthya-Desi-Kankrej-Churned-475ml/dp/B07HYXN1P9/ref=bmx_4?pd_rd_w=xLUbZ&pf_rd_p=e1cc32be-bd98-4c8f-97f9-d875240632c8&pf_rd_r=0ZF7JG5AVGZJK7RS62C3&pd_rd_r=cbe72b30-9a90-4878-8f90-c3279853bedd&pd_rd_wg=nwsfV&pd_rd_i=B07HYXN1P9&psc=1', 'EcoSvasthya Desi Kankrej A2 Pure Cow Ghee - Hand Churned High Smoke Point Hand Crafted by Traditional Vedic Process (500ml)', '', '4.0 out of 5 stars'),
(4, 'https://www.amazon.in/Vivo-Y31-Storage-Additional-Exchange/dp/B08LRDHLQM/ref=pd_rhf_dp_p_img_5?_encoding=UTF8&psc=1&refRID=YY1Y4RHFYS8J1S8P863R', 'Vivo Y31 (Racing Black, 6GB, 128GB Storage) with No Cost EMI/Additional Exchange Offers', 'https://images-eu.ssl-images-amazon.com/images/I/419PW8wuQYL._SX300_SY300_QL70_ML2_.jpg', '4.3 out of 5 stars'),
(5, 'https://www.amazon.in/Test-Exclusive_2020_1153-Multi-3GB-Storage/dp/B089MSK43J/ref=pd_vtp_6?pd_rd_w=ws2A9&pf_rd_p=51dddd6c-e5d9-4907-af32-f0b367217ee2&pf_rd_r=H4716AFCQZEG6CTB53XK&pd_rd_r=fbeeb453-c602-4190-9392-ac523dbbd400&pd_rd_wg=Vtalk&pd_rd_i=B089MSK43J&psc=1', 'Redmi Note 10 Pro (Vintage Bronze, 6GB RAM, 128GB Storage) -120Hz Super Amoled Display | 64MP Camera with 5MP Super Tele-Macro | ICICI Cashback 1000 Off', '', '4.2 out of 5 stars'),
(6, 'https://www.amazon.in/TECLUSIVE-Luxury-Stainless-Galaxy-Frontier/dp/B07V6N9CW6/ref=bmx_2?pd_rd_w=DAO1g&pf_rd_p=e1cc32be-bd98-4c8f-97f9-d875240632c8&pf_rd_r=DQN2BWAYXA0T9JKN3QYG&pd_rd_r=d8982cc0-3c29-405a-8aba-49efb5f35f90&pd_rd_wg=tnbAA&pd_rd_i=B07V6N9CW6&psc=1', 'TECLUSIVE Ultra Luxury Stainless Steel Metal 22mm Chain Strap Band for Galaxy Watch 3 (45 mm), Galaxy Watch 4.6cm, Gear S3 Frontier, Amazfit Pace Stratos, Ticwatch Pro, Huawei Watch GT', '', '4.1 out of 5 stars'),
(7, 'https://www.amazon.in/Redmi-9A-2GB-32GB-Storage/dp/B086978F2L/ref=pd_vtp_5?pd_rd_w=WVvxD&pf_rd_p=51dddd6c-e5d9-4907-af32-f0b367217ee2&pf_rd_r=1VD3THS6WE28YWP24NPF&pd_rd_r=07338b73-240c-494c-8a53-6a4cb05c2ef2&pd_rd_wg=W98kT&pd_rd_i=B086978F2L&psc=1', 'Redmi 9A (Sea Blue, 2GB Ram, 32GB Storage) | 2GHz Octa-core Helio G25 Processor', 'https://images-eu.ssl-images-amazon.com/images/I/41jRzGyDUJL._SX300_SY300_QL70_ML2_.jpg', '4.2 out of 5 stars'),
(8, 'https://www.amazon.in/Test-Exclusive239_Sep19/dp/B07XG2PKVS/ref=pd_di_sccai_5?pd_rd_w=jrWFQ&pf_rd_p=a477d781-9034-48be-b0b0-a72199c56506&pf_rd_r=9AN4MEWVDDRQJAH9HSVN&pd_rd_r=faa8461a-aeb7-4f6f-8c39-ed71c4656828&pd_rd_wg=bH4XI&pd_rd_i=B07XG2PKVS&psc=1', 'Vivo V20 Pro 5G Sunset Melody, 8GB RAM, 128GB Storage', 'https://images-eu.ssl-images-amazon.com/images/I/51dFy3SV1kL._SX300_SY300_QL70_ML2_.jpg', '4.3 out of 5 stars'),
(9, 'https://www.amazon.in/Samsung-Galaxy-Storage-Without-Offer/dp/B086KDJGVR/ref=pd_di_sccai_4?pd_rd_w=YArNL&pf_rd_p=a477d781-9034-48be-b0b0-a72199c56506&pf_rd_r=KYQZD9CWTYBZC733Z34S&pd_rd_r=85b970c0-a6c8-4bc8-97c6-3f79f69b440e&pd_rd_wg=QwJow&pd_rd_i=B086KDJGVR&psc=1', 'Samsung Galaxy A21S Blue, 6GB RAM, 64GB Storage', 'https://images-eu.ssl-images-amazon.com/images/I/41csdTL7bmL._SX300_SY300_QL70_ML2_.jpg', '3.9 out of 5 stars'),
(10, 'https://www.amazon.in/Samsung-Galaxy-Storage-Additional-Exchange/dp/B089MQ622N/ref=bmx_6?pd_rd_w=RTXd6&pf_rd_p=e1cc32be-bd98-4c8f-97f9-d875240632c8&pf_rd_r=CA95MZN18ZX547K656W5&pd_rd_r=07c3bc3e-edb0-4ed8-8b65-6dced1aa3894&pd_rd_wg=yaSmK&pd_rd_i=B089MQ622N&psc=1', 'Samsung Galaxy M01 Core (Black, 2GB RAM, 32GB Storage) with No Cost EMI/Additional Exchange Offers', 'https://images-eu.ssl-images-amazon.com/images/I/411dcyzuCzL._SX300_SY300_QL70_ML2_.jpg', '3.6 out of 5 stars'),
(11, 'https://www.amazon.in/dp/B07PXLXTCS/ref=s9_acsd_hps_bw_c2_x_0_i?pf_rd_m=A1K21FY43GMZF8&pf_rd_s=merchandised-search-3&pf_rd_r=MY3XPG80ZSVJ59VDNQ7A&pf_rd_t=101&pf_rd_p=945d1576-d29c-4a80-8a61-2d95793f25c0&pf_rd_i=16368877031', 'Amazon Brand - Solimo Water Resistant Cotton Mattress Protector 78\"x72\" - King Size, Grey', 'https://images-eu.ssl-images-amazon.com/images/I/41go3quRXSL._SX300_SY300_QL70_ML2_.jpg', '4.2 out of 5 stars');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `celery_test`
--
ALTER TABLE `celery_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_history`
--
ALTER TABLE `price_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `celery_test`
--
ALTER TABLE `celery_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `price_history`
--
ALTER TABLE `price_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
