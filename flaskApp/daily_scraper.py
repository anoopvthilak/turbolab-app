from celery import Celery
from datetime import timedelta
from celery.schedules import crontab
from bs4 import BeautifulSoup
from random import randrange
from __init__ import single_product_scrape

import mysql.connector
import requests
import datetime

app = Celery('amazon_scraper',backend="redis://localhost:6379", broker="redis://localhost:6379")
app.conf.beat_schedule = {
    'add-every-30-seconds': {
    'task': 'amazon_scraper',
	  'schedule': crontab(hour=0, minute=39)
    },
}
app.conf.timezone = 'Asia/Kolkata'

@app.task(name="amazon_scraper")
def amazon_scraper():
	mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
	mycursor = mydb.cursor(buffered=True)
	mycursor.execute("SELECT * FROM products")
	products = mycursor.fetchall()
	for prod in products:
		print ("scrape request sent for")
		print(prod[0])
		scrape_url.delay(prod)


@app.task(name="scrape_url")
def scrape_url(prod):
  product_det = single_product_scrape(prod[1])
  if not product_det:
    print("Sorry couldn't fetch")
    print(prod[0])
    return
  mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
  mycursor = mydb.cursor(buffered=True)
  product_det['low_price'] = prod[6] if prod[6] < product_det['current_price'] else product_det['current_price']
  product_det['high_price'] = prod[7] if prod[7] > product_det['current_price'] else product_det['current_price']
  mycursor.execute("UPDATE products SET current_price = %s, low_price = %s, high_price = %s WHERE id = %s ",(product_det['current_price'],product_det['low_price'],product_det['high_price'],prod[0]))
  mydb.commit()
  date = datetime.datetime.now()
  mycursor.execute("INSERT INTO price_history (product_id,price,timestamp) VALUES (%s, %s, %s)", (prod[0], product_det['current_price'],date))
  mydb.commit()
  print("inserted")
  print(prod[0])


# def fetch_product_name(data):
#     try:
#       name = data.select_one('#productTitle').text.strip()
#     except Exception:
#       return ""
#     else:
#       return name

# def fetch_price(data):
#     price = data.select_one('#priceblock_ourprice')
#     if price is None:
#         price = data.select_one('#priceblock_dealprice')
#     if price is None:
#         price = data.select_one('#priceblock_saleprice')
#     if price is None: 
#         return 0
#     price =price.text
#     price = price.replace('\u20B9','')
#     price = price.replace(',','').strip()
#     try:
#         price = float(price)
#     except Exception:
#         return 0
#     else:
#         return price

# def fetch_rating(data):
#     try:
#       rating = data.find("span", attrs={'class':'a-icon-alt'}).string.strip()
#     except Exception:
#       return ""
#     else:
#       return rating

# def fetch_image_src(data):
#     try:
#       image = data.select_one('#main-image-container img')
#       src = image['src']
#     except Exception:
#       return ""
#     else:
#       return src
#       