from celery import Celery
from datetime import timedelta
from celery.schedules import crontab
from random import randrange
from bs4 import BeautifulSoup
from random import randrange

import mysql.connector
import requests
import datetime

app = Celery('amazon_scraper',backend="redis://localhost:6379", broker="redis://localhost:6379")
app.conf.beat_schedule = {
    'add-every-30-seconds': {
    'task': 'amazon_scraper',
	'schedule': crontab(hour=21, minute=34)
    },
}
app.conf.timezone = 'Asia/Kolkata'

@app.task(name="amazon_scraper")
def amazon_scraper():
	mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
	mycursor = mydb.cursor(buffered=True)
	mycursor.execute("SELECT url,id FROM products")
	products = mycursor.fetchall()
	for prod in products:
		print ("scrape request sent for")
		print(prod[1])
		scrape_url.delay(prod[0],prod[1])


@app.task(name="scrape_ul")
def scrape_url(url,product_id):
	headers= { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:77.0) Gecko/201901'+str(randrange(10000000,99999999))+' Firefox/77.0' }
	response = requests.get(url,headers=headers)
	import html.parser
	soup_data = BeautifulSoup(response.content, 'html.parser')
	product_name = fetch_product_name(soup_data)
	price = fetch_price(soup_data)
	# if not price:
	#     return render_template('home.html',data = "Price is not available" )
	src = fetch_image_src(soup_data)
	if 'http' not in src:
	    src = ""
	rating = fetch_rating(soup_data)
	if 'stars' not in rating:
	    rating = ""
	mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
	mycursor = mydb.cursor(buffered=True)

	date = datetime.datetime.now()
	mycursor.execute("INSERT INTO price_history (product_id,price,timestamp) VALUES (%s, %s, %s)", (product_id, price,date))
	mydb.commit()

	print("inserted")
	print(product_id)


def fetch_product_name(data):
    try:
      name = data.select_one('#productTitle').text.strip()
    except Exception:
      return ""
    else:
      return name

def fetch_price(data):
    price = data.select_one('#priceblock_ourprice')
    if price is None:
        price = data.select_one('#priceblock_dealprice')
    if price is None:
        price = data.select_one('#priceblock_saleprice')
    if price is None: 
        return 0
    price =price.text
    price = price.replace('\u20B9','')
    price = price.replace(',','').strip()
    try:
        price = float(price)
    except Exception:
        return 0
    else:
        return price

def fetch_rating(data):
    try:
      rating = data.find("span", attrs={'class':'a-icon-alt'}).string.strip()
    except Exception:
      return ""
    else:
      return rating

def fetch_image_src(data):
    try:
      image = data.select_one('#main-image-container img')
      src = image['src']
    except Exception:
      return ""
    else:
      return src
      