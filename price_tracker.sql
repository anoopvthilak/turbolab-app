-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 29, 2021 at 08:38 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `price_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `price_history`
--

CREATE TABLE `price_history` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_history`
--

INSERT INTO `price_history` (`id`, `product_id`, `price`, `timestamp`) VALUES
(1, 1, 8799, '2021-04-28 18:15:18'),
(2, 2, 89490, '2021-04-28 18:16:08'),
(3, 4, 46999, '2021-04-27 19:32:28'),
(4, 5, 40380, '2021-04-27 18:58:58'),
(5, 6, 17990, '2021-04-29 12:33:25'),
(6, 5, 39499, '2021-04-29 12:42:52'),
(7, 2, 89490, '2021-04-29 12:42:52'),
(8, 4, 46999, '2021-04-29 12:42:52'),
(9, 1, 8799, '2021-04-29 12:42:52'),
(10, 6, 17990, '2021-04-29 12:42:54');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `image_src` text,
  `rating` varchar(255) DEFAULT NULL,
  `current_price` int(11) DEFAULT NULL,
  `low_price` int(11) DEFAULT NULL,
  `high_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `url`, `product_name`, `image_src`, `rating`, `current_price`, `low_price`, `high_price`) VALUES
(1, 'https://www.amazon.in/gp/product/B08696W3B3/ref=s9_acss_bw_cg_Budget_2a1_w', 'Redmi 9 (Sporty Orange, 4GB RAM, 64GB Storage)', 'https://images-eu.ssl-images-amazon.com/images/I/71A9Vo1BatL.__AC_SX300_SY300_QL70_ML2_.jpg', '4.2 out of 5 stars', 8799, 8799, 8799),
(2, 'https://www.amazon.in/Lenovo-ThinkPad-13-3-inch-Professional-20SCS01F00/dp/B087K1H42W/ref=pd_d_dss_r_4?pd_rd_w=yEtzA&pf_rd_p=4aa14100-c812-4fe3-8aef-9b1f28bd2eb7&pf_rd_r=0NBQWRNHME0CNTMT9W2S&pd_rd_r=51f37697-d4ad-478d-8062-7551b314211d&pd_rd_wg=sZBhF&pd_rd_i=B087K1H42W&psc=1', 'Lenovo ThinkPad X390 Intel Core i5 10th Gen 13.3\" FHD (1920x1080) IPS Thin and Light Laptop (8GB RAM / 512GB SSD/ Windows 10 Professional 64/ Black/ 1.29Kg), 20SCS01F00', '', '5.0 out of 5 stars', 89490, 89490, 89490),
(4, 'https://www.amazon.in/MI-Notebook-i5-10210U-Graphics-XMA1901-FK/dp/B08T9P4DQV/ref=bmx_5?pd_rd_w=Ygihk&pf_rd_p=e1cc32be-bd98-4c8f-97f9-d875240632c8&pf_rd_r=ZJG1ZCK3X81TCTPM3NKH&pd_rd_r=397a0951-3d3f-450b-af8d-7cf380113d87&pd_rd_wg=d6AC2&pd_rd_i=B08T9P4DQV&psc=1', 'MI Notebook 14 (IC) Intel Core i5-10210U 10th Gen Thin and Light Laptop(8GB/512GB SSD/Windows 10/Intel UHD Graphics/Silver/1.5Kg), XMA1901-FK', 'https://images-eu.ssl-images-amazon.com/images/I/513XIkrVeuL.__AC_SX300_SY300_QL70_ML2_.jpg', '4.1 out of 5 stars', 46999, 46999, 46999),
(5, 'https://www.amazon.in/HP-Laptop-15-inch-Windows-15s-gr0012AU/dp/B08T6THSMQ/ref=pd_vtp_4?pd_rd_w=6dQ0F&pf_rd_p=51dddd6c-e5d9-4907-af32-f0b367217ee2&pf_rd_r=WSR64EEVQYRADVH097B6&pd_rd_r=c45ef623-9961-465a-8b43-de4917bf536d&pd_rd_wg=lIIsO&pd_rd_i=B08T6THSMQ&psc=1', 'HP 15 (2021) Thin & Light Ryzen 3-3250 Laptop, 8 GB RAM, 1TB HDD + 256GB SSD, 15-inch FHD Screen, Windows 10, MS Office (15s-gr0012AU)', '', '4.2 out of 5 stars', 39499, 39499, 39499),
(6, 'https://www.amazon.in/AVITA-Essential-NE14A2INC433-MB-Integrated-Graphics/dp/B08DDZLCQF?ref_=Oct_DLandingS_D_0a7b8650_60&smid=A14CZOWI0VEHLG', 'AVITA Essential NE14A2INC433-MB 14-inch Laptop (Celeron N4000/4GB/128GB SSD/Window 10 Home in S Mode/Integrated Graphics), Matt Black', '', '3.9 out of 5 stars', 17990, 17990, 17990);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `price_history`
--
ALTER TABLE `price_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `price_history`
--
ALTER TABLE `price_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
