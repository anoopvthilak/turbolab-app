import requests
import html.parser
import asyncio,aiohttp
from random import randrange
from bs4 import BeautifulSoup
product_urls=[]
import mysql.connector
import datetime

# mydb=MySQLdb.connect.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
mydb = mysql.connector.connect(host="localhost", user="anoop", password= "anoop7563", database = "price_tracker")
mycursor = mydb.cursor(buffered=True)

def fetch_product_url(product_list_url):
	headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:77.0) Gecko/201901'+str(randrange(10000000,99999999))+' Firefox/77.0' }
	response = requests.get(product_list_url,headers=headers)
	html_data = BeautifulSoup(response.content,'html.parser')
	a_tags = html_data.select('.apb-browse-searchresults-product .a-spacing-top-small .a-link-normal')
	for a in a_tags:
		product_urls.append(a['href'])
		print(a['href'])
	# print(product_urls[2])

def search(url,idx):
    url = url_formatter(url)
    mycursor.execute("SELECT * FROM products WHERE url = '"+url+"'")
    product=mycursor.fetchone()

    if product is not None:
        mycursor.execute("SELECT * FROM price_history WHERE product_id = %s ORDER BY timestamp DESC",(product[0],))
        current_price = mycursor.fetchone()
        mycursor.execute("SELECT MIN(price) FROM price_history WHERE product_id = %s",(product[0],))
        low_price = mycursor.fetchone()
        mycursor.execute("SELECT MAX(price) FROM price_history WHERE product_id = %s",(product[0],))
        high_price = mycursor.fetchone()

        product_det = {
          "name"            : product[2],
          "url"             : product[1],
          "current_price"   : current_price[2],
          "low_price"       : low_price[0],
          "high_price"      : high_price[0],
          "src"             : product[3],
          "rating"          : product[4]
        }
    else:
        product_det=scrape_url(url,idx)
    # return render_template('index.html',product = product_det)

# for route search
def url_formatter(url):
  import re
  url = re.sub(r'[:|.|/|a-z]*in','',url)
  # url = url.split("/")
  url = "https://www.amazon.in"+url
  return url
      
def scrape_url(url,idx):
    headers= { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:77.0) Gecko/2019'+str(randrange(10000000,99999999))+' Firefox/77.0' }
    response =  requests.get(url,headers=headers)
    soup_data = BeautifulSoup(response.content, 'html.parser')
    product_name = soup_data.select_one('#productTitle').text.strip()
    price = fetch_price(soup_data)
    if not price:
        return render_template('home.html',data = "Price is not available" )
    src = fetch_image_src(soup_data)
    if 'http' not in src:
        src = ""
    rating = soup_data.find("span", attrs={'class':'a-icon-alt'}).string.strip()

    if 'stars' not in rating:
        rating = ""
    mycursor.execute("INSERT INTO products (url,product_name,image_src,rating) VALUES (%s, %s, %s, %s)", (url,product_name,src,rating))
    mydb.commit()
    product_id = int(mycursor.lastrowid);
    date = datetime.datetime.now()
    mycursor.execute("INSERT INTO price_history (product_id,price,timestamp) VALUES (%s, %s, %s)", (product_id, price,date))
    mydb.commit()
    product_det = {
      "name"            : product_name,
      "url"             : url,
      "current_price"   : price,
      "low_price"       : price,
      "high_price"      : price,
      "src"             : src,
      "rating"          : rating
    }
    # return product_det
    print("scraping completed"+str(idx))


def fetch_image_src(data):
    try:
      image = data.select_one('#main-image-container img')
      src = image['src']
    except Exception:
      return ""
    else:
      return src

def fetch_price(data):
    price = data.select_one('#priceblock_ourprice')
    if price is None:
        price = data.select_one('#priceblock_dealprice')
    if price is None:
        price = data.select_one('#priceblock_saleprice')
    if price is None: 
        return ""
    price =price.text
    price = price.replace('\u20B9','')
    price = price.replace(',','').strip()
    try:
        price = float(price)
    except Exception:
        return ""
    else:
        return price
      
url = input()
# fetch_product_url("https://www.amazon.in/s/ref=mega_elec_s23_2_2_1_2?rh=i%3Acomputers%2Cn%3A4363894031&ie=UTF8")
fetch_product_url(url)

for idx, url in enumerate(product_urls):
	print("scraping started" +str(idx))
	search(url,idx)
	

